# SceneWire
A language created by Dan Trewin.


## Description 
SceneWire is a Unity utility language that is built into a Unity Project. The project is located in the SceneWire folder. SceneWire can be incorporated into any of your own Unity Projects with a little setup. SceneWire is meant to simplify programmer workflow when modifying properties of many Unity Objects in the scene at once. For example, maybe you want to move all Unity Objects with the tag "myAwesomeTag" 2.5 units to the right. SceneWire has a great built in function for that! To see all the great functions that SceneWire offers, check out the comprehensive grammer below. 

## Screenshot
Here is some example code and an picture of how SceneWire looks integreated into a Unity Project:
![Scheme](screenshot1.png)

## Grammer
```
program  
  : statement* (EOF|ERROR_EOF)
```
```
statement  
  : PRINT expr  
  | MOVE expr expr expr expr?        
  | MOVE_TO expr expr expr expr?  
  | ROTATE expr expr expr expr?    
  | ROTATE_TO expr expr expr expr?       
  | SCALE expr expr expr expr?  
  | SCALE_TO expr expr expr expr?  
  
  | VAR IDENTIFIER INIT expr  
  | IDENFIFIER ASSIGN expr    
  | IF expr LEFT_BRACKET statement* RIGHT_BRACKET  
  | WHILE expr LEFT_BRACKET statement* RIGHT_BRACKET  
  | FUNCTION IDENTIFIER IDENTIFIER* ASSIGN LEFT_BRACKET statement* RIGHT_BRACKET  
  | CALL IDENTIFIER expr*  
```
```
expr  
  : INTEGER  
  | expr (ASTERISK|DIVIDE|MOD) expr  
  | expr (PLUS|MINUS) expr  
  | expr (MORE|MORE_OR_EQUAL|LESS|LESS_OR_EQUAL|EQUAL|NOT_EQUAL) expr  

  : DOUBLE  
  | expr (ASTERISK|DIVIDE|MOD) expr  
  | expr (PLUS|MINUS) expr  
  | expr (MORE|MORE_OR_EQUAL|LESS|LESS_OR_EQUAL|EQUAL|NOT_EQUAL) expr  

  : STRING  
  | expr PLUS expr  

  : SPECIAL/OTHER
  | COUNT_TAG expr
  | OBJ_X expr
  | OBJ_Y expr
```

## Built-in Commands

Command | Description | Example
--- | --- | ---
`PRINT` | Prints the given expr to the console. | `print 'my debug message'`
`MOVE` | Moves all game objects with the given tag an offset number of units in the x, y, and (optional) z direction. | `move 'myTag' 5.5 1.0`
`MOVE_TO` | Moves all game objects with the given tag to a given x, y, and (optional) z coordinate. | `moveTo 'myTag' 2.0 0.5`
`ROTATE` | Rotates all game objects with the given tag an offset number of units in the x, y, and (optional) z direction. | `rotate 'myTag' 90 120`
`ROTATE_TO` | Rotates all game objects with the given tag to a given x, y, and (optional) z vector. | `rotateTo 'myTag' 45 60`
`SCALE` | Scales all game objects with the given tag an offset number of units in the x, y, and (optional) z direction. | `scale 'myTag' 2 1.5`
`SCALE_TO` | Scales all game objects with the given tag to a given x, y, and (optional) z vector. | `scaleTo 'myTag' 3 3`
`COUNT_TAG` | Returns the number of game objects with the given tag (as an integer). | `countTag 'myTag'`
`OBJ_X` | Returns the x coordinate of the game object with the given name. | `objX 'awesomeObj'`
`OBJ_Y` | Returns the y coordinate of the game object with the given name. | `objY 'awesomeObj'`
