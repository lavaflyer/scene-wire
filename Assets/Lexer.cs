﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

using Token = SceneWire.Token;
using TokenType = SceneWire.TokenType;

public class Lexer {



	string source;
	List<Token> tokens;

	string tokenSoFar = "";
	int i = 0;


	public List<Token> lex (string source) {

		this.source = source;
		this.tokens = new List<Token>();

		// error handle'ing - must be an even number of quotes for strings
		int quoteCount = source.Split('\'').Length - 1;
		if (quoteCount % 2 != 0) {
			Debug.LogWarning("[Lexer Error] SceneWire can\'t lex the source code because there is an uneven number of quotes (for strings).");
			emit (TokenType.ERROR_EOF);
			return tokens;
		}

		while (i < source.Length) {

			// PLUS
			if (has (@"\+")) {
				devour ();
				emit (TokenType.PLUS);
			// MINUS
			} else if (has (@"-")) {
				devour ();
				emit (TokenType.MINUS);
			// ASTERISK
			} else if (has (@"\*")) {
				devour ();
				emit (TokenType.ASTERISK);
			// COMMENT (not emitted) or DIVIDE
			} else if (has(@"\/")) {
      			devour();
      			if (has(@"\/")) {
        			while (has(@"[^\n]")) {
          				devour();
        			}
        			tokenSoFar = "";
      			} else {
        			emit(TokenType.DIVIDE);
      			}
			// MOD
			} else if (has (@"%")) {
				devour ();
				emit (TokenType.MOD);
			// LEFT_BRACKET
			} else if (has (@"{")) {
				devour ();
				emit (TokenType.LEFT_BRACKET);
			// RIGHT_BRACKET
			} else if (has (@"}")) {
				devour ();
				emit (TokenType.RIGHT_BRACKET);
			// LEFT_PARENTHESIS or negative int or negative double
			} else if (has (@"\(")) {
				devourButSkip ();

				if (has(@"-")) {
      			
					devour(); //devours the minus sign
					while (has(@"\d")) {
						devour();
					}
					if (has(@"\.")) {
						devour();
						while (has(@"\d")) {
							devour();
						}
						emit(TokenType.DOUBLE);
					} else {
						emit(TokenType.INTEGER);		
					}
					devourButSkip(); //devours right parenthesis
				}
				else {
					artificialDevour('(');
					emit (TokenType.LEFT_PARENTHESIS);
				}
				
			// RIGHT_PARENTHESIS
			} else if (has (@"\)")) {
				devour ();
				emit (TokenType.RIGHT_PARENTHESIS);
			// NOT_EQUAL
			} else if (has (@"~")) {
				devour ();
				emit (TokenType.NOT_EQUAL);
			// EQUAL or ASSIGN
			} else if (has(@"=")) {
				devour();
				if (has(@"=")) {
					devour();
					emit(TokenType.EQUAL);
				} else {
					emit(TokenType.ASSIGN);
				}
			// MORE or MORE_OR_EQUAL
			} else if (has (@">")) {
				devour();
				if (has(@"=")) {
					devour();
					emit(TokenType.MORE_OR_EQUAL);
				} else {
					emit(TokenType.MORE);
				}
			// LESS or LESS_OR_EQUAL
			} else if (has (@"<")) {
				devour();
				if (has(@"=")) {
					devour();
					emit(TokenType.LESS_OR_EQUAL);
				} else {
					emit(TokenType.LESS);
				}
			
			// DOUBLE or INTEGER
			} else if (has(@"\d")) {
      			
				devour();
				while (has(@"\d")) {
        			devour();
      			}
				if (has(@"\.")) {
					devour();
					while (has(@"\d")) {
        				devour();
      				}
					emit(TokenType.DOUBLE);
				} else {
					emit(TokenType.INTEGER);		
				}

			// STRING - add checking for equal num '
			} else if (has(@"'")) {

				devourButSkip();
				while (has(@"[^']")) {
					devour();
				}
				devourButSkip();
				emit (TokenType.STRING);

			// IDENTIFIER or key words 
			} else if (has (@"[a-zA-Z]")) {

				devour();
				while (has (@"[a-zA-Z0-9]")) {
					devour ();
				}

				if (tokenSoFar.Equals ("while")) {
					emit (TokenType.WHILE);
				} else if (tokenSoFar.Equals ("if")) {
					emit (TokenType.IF);
				} else if (tokenSoFar.Equals ("else")) {
					emit (TokenType.ELSE);
				} else if (tokenSoFar.Equals ("end")) {
					emit (TokenType.END);
				} else if (tokenSoFar.Equals ("print")) {
					emit (TokenType.PRINT);
				} else if (tokenSoFar.Equals ("function")) {
					emit (TokenType.FUNCTION);
				} else if (tokenSoFar.Equals ("call")) {
					emit (TokenType.CALL);
				} else if (tokenSoFar.Equals ("var")) {
					emit (TokenType.VAR);
				} else if (tokenSoFar.Equals ("move")) {
					emit (TokenType.MOVE);
				} else if (tokenSoFar.Equals ("moveTo")) {
					emit (TokenType.MOVE_TO);
				} else if (tokenSoFar.Equals ("rotate")) {
					emit (TokenType.ROTATE);
				} else if (tokenSoFar.Equals ("rotateTo")) {
					emit (TokenType.ROTATE_TO);
				} else if (tokenSoFar.Equals ("scale")) {
					emit (TokenType.SCALE);
				} else if (tokenSoFar.Equals ("scaleTo")) {
					emit (TokenType.SCALE_TO);
				} else if (tokenSoFar.Equals ("countTag")) {
					emit (TokenType.COUNT_TAG);
				} else if (tokenSoFar.Equals ("objX")) {
					emit (TokenType.OBJ_X);
				} else if (tokenSoFar.Equals ("objY")) {
					emit (TokenType.OBJ_Y);
				} else {
					emit (TokenType.IDENTIFIER);
				}
			
			// white space (not emmited)
			} else if (has (@"\s")) {

				devour ();
				tokenSoFar = "";

			// anything else (quit lexing, emit ERROR_EOF)
			} else {
				Debug.LogWarning("[Lexer Error] SceneWire doesn\'t know how to interpret this: \"" + source.Substring(i) + "\"");
				emit (TokenType.ERROR_EOF);
				return tokens;
			}
		}

		//check for equal number of left and right parentheis and left and right brackets
		int leftParenCount = 0;
		int rightParenCount = 0;
		int leftBracketCount = 0;
		int rightBracketCount = 0;
		
		foreach (Token t in tokens) {

			if (t.getType() == TokenType.LEFT_PARENTHESIS) {
				leftParenCount++;
			}
			else if (t.getType() == TokenType.RIGHT_PARENTHESIS) {
				rightParenCount++;
			}
			else if (t.getType() == TokenType.LEFT_BRACKET) {
				leftBracketCount++;
			}
			else if (t.getType() == TokenType.RIGHT_BRACKET) {
				rightBracketCount++;
			}
		}
		if (leftParenCount != rightParenCount || leftBracketCount != rightBracketCount) {
			Debug.LogWarning("[Lexer Error] SceneWire can\'t lex the source code because there are an unequal number of left and right parentheses/brackets.");
			emit (TokenType.ERROR_EOF);
			return tokens;
		}


		// EOF (normal end of file)
		emit (TokenType.EOF);

		return tokens;
	}


	bool has (string pattern) {

		if (i >= source.Length) {
			return false;
		}

		Regex reg = new Regex(pattern);

		return reg.IsMatch (source [i].ToString());
	}

	void devour () {

		tokenSoFar += source [i];
		i++;
	}

	void devourButSkip () {

		i++;
	}

	void artificialDevour (char c) {

		tokenSoFar += c;
	}

	void emit (TokenType type) {

		Token token = new Token(type, tokenSoFar);

		tokens.Add (token);
		tokenSoFar = "";

		//for debugging purposes
		//Debug.Log(token);
	}
}
