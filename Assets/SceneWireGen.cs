﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SceneWireGen : MonoBehaviour {

	public SceneWire sceneWireInstance;

	public enum swfunction
	{
		print,
		move,
		moveZ,
		moveTo,
		moveToZ,
		rotate,
		rotateZ,
		rotateTo,
		rotateToZ,
		varDeclarationExample,
		functionDeclarationAndCallExample,

	}
	public swfunction function;


	public void genFunctionSkeleton () {

		if (function == swfunction.print) {
			sceneWireInstance.sceneWireSource += "\n\nprint 'Hello World!'";
		} else if (function == swfunction.move) {
			sceneWireInstance.sceneWireSource += "\n\nmove  'tag1'  3.0  0.0";
		} else if (function == swfunction.moveZ) {
			sceneWireInstance.sceneWireSource += "\n\nmove  'tag1'  3.0  0.0  0.0";
		} else if (function == swfunction.moveTo) {
			sceneWireInstance.sceneWireSource += "\n\nmoveTo  'tag1'  1.0   1.0";
		} else if (function == swfunction.moveToZ) {
			sceneWireInstance.sceneWireSource += "\n\nmoveTo  'tag1'  1.0  1.0  0.0";
		} else if (function == swfunction.rotate) {
			sceneWireInstance.sceneWireSource += "\n\nrotate  'tag1'  35  0";
		} else if (function == swfunction.rotateZ) {
			sceneWireInstance.sceneWireSource += "\n\nrotate  'tag1'  0  0  45";
		} else if (function == swfunction.rotateTo) {
			sceneWireInstance.sceneWireSource += "\n\nrotateTo  'tag1'  35  0";
		} else if (function == swfunction.rotateToZ) {
			sceneWireInstance.sceneWireSource += "\n\nrotateTo  'tag1'  0  0  45";
		} else if (function == swfunction.varDeclarationExample) {
			sceneWireInstance.sceneWireSource += "\n\nvar x = 7";
		} else if (function == swfunction.functionDeclarationAndCallExample) {
			sceneWireInstance.sceneWireSource += "\n\nfunction dan x y = {\n\n   var tag1 = 'tag1'\n   move tag1 x y\n}\n\ncall dan 2 3";
		}
	}
}


[CustomEditor(typeof(SceneWireGen))]
public class SceneWireGenEditor : Editor
{

	public override void OnInspectorGUI() {

		SceneWireGen myScript = (SceneWireGen)target;


		DrawDefaultInspector ();	

		if (GUILayout.Button ("Generate Function Skeleton", GUILayout.Height (30))) {

			myScript.genFunctionSkeleton ();
		}

	}


}
