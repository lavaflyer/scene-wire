﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

using Token = SceneWire.Token;
using TokenType = SceneWire.TokenType;
using sw = Ast;

public class Parser {

	List<Token> tokens;
	int i = 0;


	string parserError = "";


	public sw.Block parse (List<Token> tokens) {
		
		this.tokens = tokens;
		List<sw.Statement> statements = new List<sw.Statement>();

		//lexer error
		foreach (Token token in tokens) {
			if (token.getType () == TokenType.ERROR_EOF) {
				parserError = "[Parser Error] SceneWire can\'t run this code because the Lexer had a problem.";
			}
		}

		
		
		//normal path
		while (!has(TokenType.EOF) && parserError == "") {
      		statements.Add( statement() );
   	 	}

		//error path
		if (parserError != "") {
			statements.Clear ();
			sw.Statement onlyStatement = new sw.PrintStatement (new sw.StringLiteralExpression(parserError), 1);
			statements.Add (onlyStatement);
		}
   	 	
		return new sw.Block(statements);
	}


	public bool has(TokenType type) {
    	return i < tokens.Count && tokens[i].getType() == type;
  	}

	public Token devour() {
    	Token token = tokens[i];
    	i++;
    	return token;
  	}

	public sw.Statement statement() {

		// MOVE
		if (has (TokenType.MOVE)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xAmt = expression ();
			sw.Expression yAmt = expression ();
			sw.Expression zAmt = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zAmt = expression ();
			}

			return new sw.MoveStatement (tag, xAmt, yAmt, zAmt);

		// MOVE_TO
		} else if (has (TokenType.MOVE_TO)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xPos = expression ();
			sw.Expression yPos = expression ();
			sw.Expression zPos = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zPos = expression ();
			}

			return new sw.MoveToStatement (tag, xPos, yPos, zPos);

		// ROTATE
		} else if (has (TokenType.ROTATE)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xAmt = expression ();
			sw.Expression yAmt = expression ();
			sw.Expression zAmt = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zAmt = expression ();
			}

			return new sw.RotateStatement (tag, xAmt, yAmt, zAmt);

		// ROTATE_TO
		} else if (has (TokenType.ROTATE_TO)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xPos = expression ();
			sw.Expression yPos = expression ();
			sw.Expression zPos = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zPos = expression ();
			}

			return new sw.RotateToStatement (tag, xPos, yPos, zPos);

		// SCALE
		} else if (has (TokenType.SCALE)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xAmt = expression ();
			sw.Expression yAmt = expression ();
			sw.Expression zAmt = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zAmt = expression ();
			}

			return new sw.ScaleStatment (tag, xAmt, yAmt, zAmt);
	
		// SCALE_TO
		} else if (has (TokenType.SCALE_TO)) {
			devour ();

			sw.Expression tag = expression ();
			sw.Expression xPos = expression ();
			sw.Expression yPos = expression ();
			sw.Expression zPos = new sw.Expression(); //NULL expression
			
			if (has (TokenType.INTEGER) || has (TokenType.DOUBLE)) {
				zPos = expression ();
			}

			return new sw.ScaleToStatement (tag, xPos, yPos, zPos);

		// PRINT
		} else if (has (TokenType.PRINT)) {
			devour ();

			sw.Expression printVal = expression ();

			return new sw.PrintStatement (printVal);

		// VAR
		} else if (has (TokenType.VAR)) {
			devour ();

			Token id = null;
			if (has (TokenType.IDENTIFIER)) {
				id = devour ();

				if (Ast.dataNamesUsed.Contains(id.getSource())) {
					//error
					parserError = "[Parser Error] SceneWire can\'t init another variable of name \"" + id.getSource() + "\"";
					return new sw.Statement ();
				}
				Ast.dataNamesUsed.Add (id.getSource ());
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects variable init to follow format: \"var name = 2\"";
				return new sw.Statement ();
			}

			if (has (TokenType.ASSIGN)) {
				devour ();
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects variable init to follow format: \"var name = 2\"";
				return new sw.Statement ();
			}
			sw.Expression value = expression ();

			return new sw.VariableAssignmentStatement (id.getSource(), value);


		// IDENTIFIER
		} else if (has (TokenType.IDENTIFIER)) {
			
			Token id = devour ();

			if (!Ast.dataNamesUsed.Contains(id.getSource())) {
				//error
				parserError = "[Parser Error] A variable of name \"" + id.getSource() + "\" was never initialized.";
				return new sw.Statement ();
			}

			if (has (TokenType.ASSIGN)) {
				devour ();
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects variable assignment to follow format: \"name = 2\"";
				return new sw.Statement ();
			}
			sw.Expression value = expression ();

			return new sw.VariableAssignmentStatement (id.getSource(), value);
		
		// FUNCTION
		} else if (has (TokenType.FUNCTION)) {
			devour();

			Token id = null;
			if (has (TokenType.IDENTIFIER)) {
				id = devour ();

				if (Ast.dataNamesUsed.Contains(id.getSource())) {
					//error
					parserError = "[Parser Error] SceneWire can\'t init another function of name \"" + id.getSource() + "\"";
					return new sw.Statement ();
				}
				Ast.dataNamesUsed.Add (id.getSource ());
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects function init to follow format: \"function name param1 param2 = { //code }\"";
				return new sw.Statement ();
			}

			List<string> formals = new List<string>();
			while (has(TokenType.IDENTIFIER)) {
				Token formalToken = devour();
				formals.Add(formalToken.getSource());
			}

			if (has (TokenType.ASSIGN)) {
				devour ();
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects function init to follow format: \"function name param1 param2 = { //code }\"";
				return new sw.Statement ();
			}

			if (has (TokenType.LEFT_BRACKET)) {
				devour ();
			} else {
				//error
				parserError = "[Parser Error] SceneWire expects function init to follow format: \"function name param1 param2 = { //code }\"";
				return new sw.Statement ();
			}

			List<sw.Statement> statements = new List<sw.Statement>();
			while (!has(TokenType.RIGHT_BRACKET)) {
				statements.Add(statement());
			}
			devour (); // eat RIGHT_BRACKET


			return new sw.FunctionAssignmentStatement (id.getSource(), formals, new sw.Block(statements));

		// CALL
		} else if (has (TokenType.CALL)) {
			devour();

			Token id = devour ();

			if (!Ast.dataNamesUsed.Contains(id.getSource())) {
				//error
				parserError = "[Parser Error] A function of name \"" + id.getSource() + "\" was never initialized.";
				return new sw.Statement ();
			}

			List<sw.Expression> expressions = new List<sw.Expression>();
			bool done = false;
			while (done == false) {
				sw.Expression e = expression();
				if (e.evaluate() == "NULL") {
					done = true;
				}
				else {
					expressions.Add(e);
				}
			}

			return new sw.FunctionReferenceStatement (id.getSource(), expressions);


		// WHILE
		} else if (has (TokenType.WHILE)) {	

			return loop();	

		// IF
		} else if (has (TokenType.IF)) {
 			
			 return conditional();

		} else {


			devour ();
			return new sw.Statement ();
		}
	}


	public sw.Statement loop() {

		devour(); //devours while

		sw.Expression condition = expression();
		List<sw.Statement> statements = new List<sw.Statement>();
		
		devour(); //devours left bracket
		while (!has(TokenType.RIGHT_BRACKET)) {
			statements.Add(statement());
		}
		devour(); //devours right bracket

		return new sw.WhileStatement (condition, new sw.Block(statements));
	}

	public sw.Statement conditional() {

		devour(); //devours if

		sw.Expression condition = expression();
		List<sw.Statement> thenStatements = new List<sw.Statement>();
		
		devour(); //devours left bracket

		while (!has(TokenType.RIGHT_BRACKET)) {
    		thenStatements.Add(statement());
  		}
		devour(); //devours right bracket


		return new sw.IfStatement (condition, new sw.Block(thenStatements));

	}

	
	public sw.Expression expression() {

		//special expressions
		if (has(TokenType.COUNT_TAG)) {
			devour();
			return new sw.CountTagExpression(atom());
		}
		else if (has(TokenType.OBJ_X)) {
			devour();
			return new sw.ObjXExpression(atom());
		}
		else if (has(TokenType.OBJ_Y)) {
			devour();
			return new sw.ObjYExpression(atom());
		}


		return relational();
	}

	public sw.Expression relational() {
		
		sw.Expression valA = additive();

		while (has(TokenType.MORE_OR_EQUAL) || has(TokenType.MORE) || has(TokenType.LESS_OR_EQUAL) || has(TokenType.LESS) || has(TokenType.EQUAL) || has(TokenType.NOT_EQUAL)) {
		
			Token op = devour(); //operator token

			sw.Expression valB = additive();

			if (op.getType() == TokenType.MORE_OR_EQUAL) {
				valA = new sw.MoreOrEqualExpression(valA, valB);
			} else if (op.getType() == TokenType.MORE) {
				valA = new sw.MoreExpression(valA, valB);
			} else if (op.getType() == TokenType.LESS_OR_EQUAL) {
				valA = new sw.LessOrEqualExpression(valA, valB);
			} else if (op.getType() == TokenType.LESS) {
				valA = new sw.LessExpression(valA, valB);
			} else if (op.getType() == TokenType.EQUAL) {
				valA = new sw.EqualExpression(valA, valB);
			} else {
				valA = new sw.NotEqualExpression(valA, valB);
			}

		}
		//could have no comparisons
		return valA;
	}

	public sw.Expression additive() {

		sw.Expression left = multiplicative();

		while (has(TokenType.PLUS) || has(TokenType.MINUS)) {
			
			Token op = devour(); //operator token
			sw.Expression right = multiplicative();

			if (op.getType() == TokenType.PLUS) {
				left = new sw.AddExpression(left, right);
			}
			else {
        		left = new sw.SubtractExpression(left, right);
      		}
		}

		return left;
	}

	public sw.Expression multiplicative() {

		sw.Expression left = atom();

		while (has(TokenType.ASTERISK) || has(TokenType.DIVIDE) || has(TokenType.MOD)) {

			Token op = devour(); //operator token
			sw.Expression right = atom();

			 if (op.getType() == TokenType.ASTERISK) {
				left = new sw.MultiplyExpression(left, right);
			} else if (op.getType() == TokenType.MOD) {
				left = new sw.ModExpression(left, right);
			} else {
				left = new sw.DivideExpression(left, right);
			}
		}
		return left;
	}

	//base expression
	public sw.Expression atom() {

		if (has (TokenType.INTEGER)) {
			Token token = devour ();

			return new sw.IntegerLiteralExpression (int.Parse (token.getSource ()));

		} else if (has (TokenType.DOUBLE)) {
			Token token = devour ();

			return new sw.DoubleLiteralExpression (float.Parse (token.getSource ()));

		} else if (has (TokenType.STRING)) {
			Token token = devour ();

			return new sw.StringLiteralExpression (token.getSource());

		} else if (has (TokenType.IDENTIFIER)) {
			Token token = devour ();

			return new sw.VariableReferenceExpression (token.getSource());

		} else if (has(TokenType.LEFT_PARENTHESIS)) {
     		devour(); //devours left parenthesis
			sw.Expression e = expression();
			devour(); //devours right parenthesis

			return e;

		} else {

			return new sw.Expression ();
		}
	}
	
}
