﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using System;

public class Ast {

	public static List<string> dataNamesUsed; // includes both variables and functions (ie variables & functions cant have the same name!)

	public static Dictionary<string, string> variables;

	public static Dictionary<string, Block> functions;
	public static Dictionary<string, List<string>> functionFormals;

	public class Statement {

		public Statement() {

		}

		public virtual void evaluate() {
			Debug.LogWarning ("[Ast Error] SceneWire evaluated an empty statement!");
		}
	}

	public class Block : Statement {
		List<Statement> statements;

		public Block(List<Statement> statements) {
			this.statements = statements;
		}

		public List<Statement> getStatements() {
			return statements;
		}

		public override void evaluate() {
			
			foreach (Statement statement in statements) {
				
				statement.evaluate();
			}
		}

		public override string ToString() {
			string ret = "";
			foreach (Statement st in statements) {
				ret += st.ToString() + ";\n"; 
			}
			return ret;
		}
	}

	public class MoveStatement : Statement {
		Expression tag;
		Expression xAmt;
		Expression yAmt;
		Expression zAmt;

		public MoveStatement(Expression tag, Expression xAmt, Expression yAmt, Expression zAmt) {
			this.tag = tag;
			this.xAmt = xAmt;
			this.yAmt = yAmt;
			this.zAmt = zAmt;
		}

		public override void evaluate() {
			
			string tag2 = tag.evaluate ();
			float xAmt2 = float.Parse(xAmt.evaluate());
			float yAmt2 = float.Parse(yAmt.evaluate());
			float zAmt2 = 0.0f;
			
			if (zAmt.evaluate() != "NULL") { //zAmt is explicitly given
				zAmt2 = float.Parse(zAmt.evaluate());
			}
			
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {
				go.transform.position += new Vector3 (xAmt2, yAmt2, zAmt2);
			}
		}

		public override string ToString() {
			return "MoveStatement";
		}
	}

	public class MoveToStatement : Statement {
		Expression tag;
		Expression xPos;
		Expression yPos;
		Expression zPos;

		public MoveToStatement(Expression tag, Expression xPos, Expression yPos, Expression zPos) {
			this.tag = tag;
			this.xPos = xPos;
			this.yPos = yPos;
			this.zPos = zPos;
		}

		public override void evaluate() {

			string tag2 = tag.evaluate ();
			float xPos2 = float.Parse(xPos.evaluate());
			float yPos2 = float.Parse(yPos.evaluate());
			float zPos2 = 0.0f;

			bool zPosGiven = false;
			if (zPos.evaluate() != "NULL") { //zPos is explicitly given
				zPos2 = float.Parse(zPos.evaluate());
				zPosGiven = true;
			}

			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {
				
				if (zPosGiven) {
					go.transform.position = new Vector3 (xPos2, yPos2, zPos2);
				}
				else {
					zPos2 = go.transform.position.z;
					go.transform.position = new Vector3 (xPos2, yPos2, zPos2);
				}
			}
		}

		public override string ToString() {
			return "MoveToStatement";
		}
	}

	public class RotateStatement : Statement {
		Expression tag;
		Expression xAmt;
		Expression yAmt;
		Expression zAmt;

		public RotateStatement(Expression tag, Expression xAmt, Expression yAmt, Expression zAmt) {
			this.tag = tag;
			this.xAmt = xAmt;
			this.yAmt = yAmt;
			this.zAmt = zAmt;
		}

		public override void evaluate() {
			
			string tag2 = tag.evaluate ();
			float xAmt2 = float.Parse(xAmt.evaluate());
			float yAmt2 = float.Parse(yAmt.evaluate());
			float zAmt2 = 0.0f;
			
			if (zAmt.evaluate() != "NULL") { //zAmt is explicitly given
				zAmt2 = float.Parse(zAmt.evaluate());
			}
			
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {
				go.transform.Rotate(xAmt2, yAmt2, zAmt2);
			}
		}

		public override string ToString() {
			return "RotateStatement";
		}
	}

	public class RotateToStatement : Statement {
		Expression tag;
		Expression xPos;
		Expression yPos;
		Expression zPos;

		public RotateToStatement(Expression tag, Expression xPos, Expression yPos, Expression zPos) {
			this.tag = tag;
			this.xPos = xPos;
			this.yPos = yPos;
			this.zPos = zPos;
		}

		public override void evaluate() {
			
			string tag2 = tag.evaluate ();
			float xPos2 = float.Parse(xPos.evaluate());
			float yPos2 = float.Parse(yPos.evaluate());
			float zPos2 = 0.0f;

			bool zPosGiven = false;
			if (zPos.evaluate() != "NULL") { //zPos is explicitly given
				zPos2 = float.Parse(zPos.evaluate());
				zPosGiven = true;
			}
			
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {

				if (zPosGiven) {
					go.transform.eulerAngles = new Vector3(xPos2, yPos2, zPos2);
				}
				else {
					zPos2 = go.transform.eulerAngles.z;
					go.transform.eulerAngles = new Vector3(xPos2, yPos2, zPos2);
				}
			}
		}

		public override string ToString() {
			return "RotateToStatement";
		}
	}

	public class ScaleStatment : Statement {
		Expression tag;
		Expression xAmt;
		Expression yAmt;
		Expression zAmt;

		public ScaleStatment(Expression tag, Expression xAmt, Expression yAmt, Expression zAmt) {
			this.tag = tag;
			this.xAmt = xAmt;
			this.yAmt = yAmt;
			this.zAmt = zAmt;
		}

		public override void evaluate() {
			
			string tag2 = tag.evaluate ();
			float xAmt2 = float.Parse(xAmt.evaluate());
			float yAmt2 = float.Parse(yAmt.evaluate());
			float zAmt2 = 0.0f;
			
			if (zAmt.evaluate() != "NULL") { //zAmt is explicitly given
				zAmt2 = float.Parse(zAmt.evaluate());
			}
			
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {
				go.transform.localScale += new Vector3(xAmt2,yAmt2,zAmt2);
			}
		}

		public override string ToString() {
			return "ScaleStatment";
		}
	}
	
	public class ScaleToStatement : Statement {
		Expression tag;
		Expression xPos;
		Expression yPos;
		Expression zPos;

		public ScaleToStatement(Expression tag, Expression xPos, Expression yPos, Expression zPos) {
			this.tag = tag;
			this.xPos = xPos;
			this.yPos = yPos;
			this.zPos = zPos;
		}

		public override void evaluate() {
			
			string tag2 = tag.evaluate ();
			float xPos2 = float.Parse(xPos.evaluate());
			float yPos2 = float.Parse(yPos.evaluate());
			float zPos2 = 0.0f;

			bool zPosGiven = false;
			if (zPos.evaluate() != "NULL") { //zPos is explicitly given
				zPos2 = float.Parse(zPos.evaluate());
				zPosGiven = true;
			}
			
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {

				if (zPosGiven) {
					go.transform.localScale = new Vector3(xPos2, yPos2, zPos2);
				}
				else {
					zPos2 = go.transform.localScale.z;
					go.transform.localScale = new Vector3(xPos2, yPos2, zPos2);
				}
			}
		}

		public override string ToString() {
			return "ScaleToStatment";
		}
	}

	public class VariableAssignmentStatement : Statement {
		string id;
		Expression expression;

		public VariableAssignmentStatement(string id, Expression expression) {
			this.id = id;
			this.expression = expression;
		}

		public override void evaluate () {
			setVariable (id, expression);
		}

		public override string ToString() {
			return "VariableAssignmentStatement";
		}
	}

	public class FunctionAssignmentStatement : Statement {
		string id;
		List<string> formals;
		Block block;

		public FunctionAssignmentStatement(string id, List<string> formals, Block block) {
			this.id = id;
			this.block = block;
			this.formals = formals;
		}

		public override void evaluate () {
			setFunction (id, block);
			setFunctionFormals (id, formals);
		}

		public override string ToString() {
			return "FunctionAssignmentStatement";
		}
	}

	public class FunctionReferenceStatement : Statement {
		string id;
		List<Expression> actuals;

		public FunctionReferenceStatement(string id, List<Expression> actuals) {
			this.id = id;
			this.actuals = actuals;
		}

		public override void evaluate() {

			List<string> formals = getFunctionFormals(id);

			for (int i = 0; i < actuals.Count; i++) {
				//setVariable (formals[i], actuals[i]);
				new VariableAssignmentStatement(formals[i], actuals[i]).evaluate();
			}
			getFunction(id).evaluate();
		}

		public override string ToString() {
			return "FunctionReferenceStatement";
		}
	}

	public class WhileStatement : Statement {
		Expression condition;
		Block block;

		public WhileStatement(Expression condition, Block block) {
			this.condition = condition;
			this.block = block;
		}

		public override void evaluate() {

			while (condition.evaluate() != "0") {
				block.evaluate();
			}
		}

		public override string ToString() {
			return "WhileStatement";
		}
	}

	public class IfStatement : Statement {
		Expression condition;
		Block block;

		public IfStatement(Expression condition, Block block) {
			this.condition = condition;
			this.block = block;
		}

		public override void evaluate() {

			if (condition.evaluate() != "0") {
				block.evaluate();
			}
		}

		public override string ToString() {
			return "IfStatement";
		}
	}

	public class PrintStatement : Statement {
		Expression message;
		int typeOfMessage;

		public PrintStatement(Expression message, int typeOfMessage = 0) {
			this.message = message;
			this.typeOfMessage = typeOfMessage;
		}

		public override void evaluate() {
			if (typeOfMessage == 0) {
				Debug.Log (message.evaluate());
			} else {
				Debug.LogWarning (message.evaluate());
			}
		}

		public override string ToString() {
			return "PrintStatement";
		}
	}


	public class Expression {
		

		public Expression() {

		}

		public virtual string evaluate() {
			return "NULL";
		}
	}

	public class IntegerLiteralExpression : Expression {
		int val;

		public IntegerLiteralExpression(int val) {
			this.val = val;
		}

		// returning int
		public override string evaluate() {

			return val.ToString ();
		}
	}

	public class DoubleLiteralExpression : Expression {
		float val;

		public DoubleLiteralExpression(float val) {
			this.val = val;
		}

		// returning float
		public override string evaluate() {

			return val.ToString ();
		}
	}

	public class StringLiteralExpression : Expression {
		string val;

		public StringLiteralExpression(string val) {
			this.val = val;
		}

		// returning string
		public override string evaluate() {

			return val.ToString ();
		}
	}

	public class VariableReferenceExpression : Expression {
		string id;

		public VariableReferenceExpression(string id) {
			this.id = id;
		}

		// returning string
		public override string evaluate() {

			return getVariable(id);
		}
	}

	public class AddExpression : Expression {

		Expression left;
		Expression right;

		public AddExpression(Expression left, Expression right) {
			this.left = left;
			this.right = right;
		}

		public override string evaluate() {
			string leftSt = left.evaluate();
			string rightSt = right.evaluate();

			try {
				float leftVal = float.Parse(leftSt);
				float rightVal = float.Parse(rightSt);

				float returnVal = leftVal + rightVal;
				return returnVal.ToString();
			}
			catch {
				//any exception where the values can't be altered (ie float + string or sting + string)
				return leftSt + rightSt;
			}
		}
	}

	public class SubtractExpression : Expression {

		Expression left;
		Expression right;

		public SubtractExpression(Expression left, Expression right) {
			this.left = left;
			this.right = right;
		}

		public override string evaluate() {
			string leftSt = left.evaluate();
			string rightSt = right.evaluate();

			try {
				float leftVal = float.Parse(leftSt);
				float rightVal = float.Parse(rightSt);

				float returnVal = leftVal - rightVal;
				return returnVal.ToString();
			}
			catch {
				//any exception where the values can't be altered (ie float - string or sting - string)
				return "NULL";
			}
		}
	}

	public class MultiplyExpression : Expression {

		Expression left;
		Expression right;

		public MultiplyExpression(Expression left, Expression right) {
			this.left = left;
			this.right = right;
		}

		public override string evaluate() {
			string leftSt = left.evaluate();
			string rightSt = right.evaluate();

			try {
				float leftVal = float.Parse(leftSt);
				float rightVal = float.Parse(rightSt);

				float returnVal = leftVal * rightVal;
				return returnVal.ToString();
			}
			catch {
				//any exception where the values can't be altered (ie float * string or sting * string)
				return "NULL";
			}
		}
	}

	public class DivideExpression : Expression {

		Expression left;
		Expression right;

		public DivideExpression(Expression left, Expression right) {
			this.left = left;
			this.right = right;
		}

		public override string evaluate() {
			string leftSt = left.evaluate();
			string rightSt = right.evaluate();

			try {
				float leftVal = float.Parse(leftSt);
				float rightVal = float.Parse(rightSt);

				float returnVal = leftVal / rightVal;
				return returnVal.ToString();
			}
			catch {
				//any exception where the values can't be altered (ie float / string or sting / string)
				return "NULL";
			}
		}
	}

	public class ModExpression : Expression {

		Expression left;
		Expression right;

		public ModExpression(Expression left, Expression right) {
			this.left = left;
			this.right = right;
		}

		public override string evaluate() {
			string leftSt = left.evaluate();
			string rightSt = right.evaluate();

			try {
				float leftVal = float.Parse(leftSt);
				float rightVal = float.Parse(rightSt);

				float returnVal = leftVal % rightVal;
				return returnVal.ToString();
			}
			catch {
				//any exception where the values can't be altered (ie float % string or sting % string)
				return "NULL";
			}
		}
	}

	public class MoreExpression : Expression {

		Expression a;
		Expression b;

		public MoreExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat > bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class MoreOrEqualExpression : Expression {

		Expression a;
		Expression b;

		public MoreOrEqualExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat >= bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class LessExpression : Expression {

		Expression a;
		Expression b;

		public LessExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat < bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class LessOrEqualExpression : Expression {

		Expression a;
		Expression b;

		public LessOrEqualExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat <= bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class EqualExpression : Expression {

		Expression a;
		Expression b;

		public EqualExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat == bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class NotEqualExpression : Expression {

		Expression a;
		Expression b;

		public NotEqualExpression(Expression a, Expression b) {
			this.a = a;
			this.b = b;
		}

		public override string evaluate() {
			string valA = a.evaluate();
			string valB = b.evaluate();

			try {
				float aFloat = float.Parse(valA);
				float bFloat = float.Parse(valB);

				bool returnVal = aFloat != bFloat;
				if (returnVal == true) {
					return "1";
				}
				else {
					return "0";
				}
			}
			catch {
				//any exception where the values can't be compared (ie string > string) return false!!
				return "0";
			}
		}
	}

	public class CountTagExpression : Expression {
		Expression tag;

		public CountTagExpression(Expression tag) {
			this.tag = tag;
		}

		public override string evaluate() {
			
			string tag2 = tag.evaluate ();
			
			int count = 0;
			foreach (GameObject go in GameObject.FindGameObjectsWithTag(tag2)) {

				count++;
			}
			return count.ToString();
		}
	}
	public class ObjXExpression : Expression {
		Expression objName;

		public ObjXExpression(Expression objName) {
			this.objName = objName;
		}

		public override string evaluate() {
			
			string objName2 = objName.evaluate ();

			GameObject go = GameObject.Find(objName2);
			
			if (go != null) {
				return go.transform.position.x.ToString();
			}
			return "-1";
		}
	}

	public class ObjYExpression : Expression {
		Expression objName;

		public ObjYExpression(Expression objName) {
			this.objName = objName;
		}

		public override string evaluate() {
			
			string objName2 = objName.evaluate ();

			GameObject go = GameObject.Find(objName2);
			
			if (go != null) {
				return go.transform.position.y.ToString();
			}
			return "-1";
		}
	}




	public static void setVariable(string id, Expression expression) {

		variables [id] = expression.evaluate ();
	}
	public static string getVariable(string id) {

		if (variables.ContainsKey (id)) {
			return variables [id];
		} else {
			return null;
		}
	}

	public static void setFunction(string id, Block block) {

		functions [id] = block;
	}
	public static Block getFunction(string id) {

		if (functions.ContainsKey (id)) {
			return functions [id];
		} else {
			return null;
		}
	}

	public static void setFunctionFormals(string id, List<string> names) {

		functionFormals ["$" + id] = names;
	}
	public static List<string> getFunctionFormals(string id) {

		if (functionFormals.ContainsKey ("$" + id)) {
			return functionFormals ["$" + id];
		} else {
			return null;
		}
	}



	 
}
