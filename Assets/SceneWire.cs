﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using sw = Ast;

public class SceneWire : MonoBehaviour {


	public class Token {

		TokenType type;
		string source;

		public Token (TokenType type, string source) {
			this.type = type;
			this.source = source;
		}

		public TokenType getType() {
			return type;
		}

		public string getSource() {
			return source;
		}

		public override string ToString () {
			return "Token : { " + type + " \"" + source + "\" }";
		}
	}

	public enum TokenType {

		IDENTIFIER,
		PLUS,
		EOF,
		ERROR_EOF,
		LEFT_PARENTHESIS,
		RIGHT_PARENTHESIS,
		LEFT_BRACKET,
		RIGHT_BRACKET,
		DIVIDE,
		MINUS,
		ASTERISK,
		COMMA,
		MOD,
		INTEGER,
		DOUBLE,
		STRING,
		WHILE,
		IF,
		ELSE,
		END,
		MORE_OR_EQUAL,
		MORE,
		LESS_OR_EQUAL,
		LESS,
		EQUAL,
		NOT_EQUAL,
		ASSIGN,
		VAR,
		FUNCTION,
		CALL,

		//built in methods / statments
		PRINT,
		MOVE,
		MOVE_TO,
		ROTATE,
		ROTATE_TO,
		SCALE,
		SCALE_TO,

		//built in expressions
		COUNT_TAG,
		OBJ_X,
		OBJ_Y
	}




    [TextArea(20,20)]
    public string sceneWireSource = "This is the default source code";

	public void runClicked () {

		Ast.dataNamesUsed = new List<string> ();

		Ast.variables = new Dictionary<string, string> ();
		Ast.functions = new Dictionary<string, sw.Block> ();
		Ast.functionFormals = new Dictionary<string, List<string>> ();

		//Debug.Log("Run clicked - Source: " + sceneWireSource);

	
		Lexer lexer = new Lexer ();
		List<Token> tokens = lexer.lex(sceneWireSource);


		//temp for debugging
		//foreach (Token token in tokens) {
			//Debug.Log (token);
		//}


		Parser parser = new Parser ();
		sw.Block ast = parser.parse (tokens);


		//foreach (sw.Statement statement in ast.getStatements()) {

		//}

		ast.evaluate ();

	}
}
