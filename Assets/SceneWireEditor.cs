﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(SceneWire))]
public class SceneWireEditor : Editor
{
	
	public override void OnInspectorGUI() {

		SceneWire myScript = (SceneWire)target;
        

		DrawDefaultInspector();	



		//GUILayout.Label ("SceneWire Errors: (None)", GUILayout.Height (60));

	
		GUILayout.BeginHorizontal("box");
		if(GUILayout.Button("Save1")) {
		
			PlayerPrefs.SetString ("Save1", myScript.sceneWireSource);
		}
		if(GUILayout.Button("Load1")) {

			if (PlayerPrefs.GetString ("Save1") != null) {
				myScript.sceneWireSource = PlayerPrefs.GetString ("Save1");
			}
		}
		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal("box");
		if(GUILayout.Button("Save2")) {

			PlayerPrefs.SetString ("Save2", myScript.sceneWireSource);
		}
		if(GUILayout.Button("Load2")) {

			if (PlayerPrefs.GetString ("Save2") != null) {
				myScript.sceneWireSource = PlayerPrefs.GetString ("Save2");
			}
		}
		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal("box");
		if(GUILayout.Button("Save3")) {

			PlayerPrefs.SetString ("Save3", myScript.sceneWireSource);
		}
		if(GUILayout.Button("Load3")) {

			if (PlayerPrefs.GetString ("Save3") != null) {
				myScript.sceneWireSource = PlayerPrefs.GetString ("Save3");
			}
		}
		GUILayout.EndHorizontal ();



		if(GUILayout.Button("Run", GUILayout.Height(60))) {
			
			myScript.runClicked();
        }

	}
}